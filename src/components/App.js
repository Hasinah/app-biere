import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Beer from './Beer';

const App = () => {
  const [beer, setBeer] = useState();
  const [error, setError] = useState();

  useEffect(() => {
    axios
      .get('https://api.punkapi.com/v2/beers/random')
      .then((response) => {
        const fetchedBeer = response.data[0];
        setBeer(fetchedBeer);
      })
      .catch((err) => setError(error));
  }, []);

  if (!beer) return <div>Chargement...</div>;

  return (
    <div>
      <Beer {...beer} />
      {error && <div style={{color: 'red'}}>{error}</div>}
    </div>
  );
};

export default App;
//formulaire
// import React, {useState} from 'react';
// import Input from './Input';

// const Form = () => {
//   const [name, setName] = useState('');
//   const [email, setEmail] = useState('');
//   const [error, setError] = useState();

//   const onSubmit = () => {
//     setError(null);
//     if (!name || name === '')
//       setError({field: 'name', message: "Merci d'entrer un nom"});
//     else if (!email)
//       setError({field: 'email', message: "Merci d'entrer un email valide"});
//     else console.log("Vous êtes identifié ! Chargement de l'application...");
//   };

//   return (
//     <div>
//       <Input
//         value={name}
//         setValue={setName}
//         label="Name"
//         error={error ? error.field === 'name' : false}
//       />
//       <Input
//         value={email}
//         setValue={setEmail}
//         label="Email"
//         error={error ? error.field === 'email' : false}
//       />
//       {error && <div>Error: {error}</div>}
//       <button onClick={onSubmit}>Submit</button>
//     </div>
//   );
// };

// export default Form;
