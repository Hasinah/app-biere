// index.js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

try {
  ReactDOM.render(<App />, document.getElementById('root'));
} catch (error) {
  console.log({error});
}

module.hot.accept();
