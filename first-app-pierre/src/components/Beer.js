import React from 'react';

const Beer = ({name, image_url, description, id}) => (
  <div>
    <h3>{name}</h3>
    <p>{description}</p>
    <img src={image_url} alt={id} />
  </div>
);

export default Beer;
