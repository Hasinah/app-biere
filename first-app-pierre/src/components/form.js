import React from 'react';
import Input from './Input';

const Form = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [error, setError] = useState();

  const onSubmit = () => {
    setError(null);
    if (!name || name === '')
      setError({field: 'name', message: "Merci d'entrer un nom"});
    else if (!email || !isEmail(email))
      setError({field: 'email', message: "Merci d'entrer un email valide"});
    else console.log("Vous êtes identifié ! Chargement de l'application...");
  };

  return (
    <div>
      <Input
        value={name}
        setValue={setName}
        label="Name"
        error={error?.field === 'name'}
      />
      <Input
        value={email}
        setValue={setEmail}
        label="Email"
        error={error?.field === 'email'}
      />
      {error && <div>Error: {error}</div>}
      <button onClick={onSubmit}>Submit</button>
    </div>
  );
};

export default Form;
