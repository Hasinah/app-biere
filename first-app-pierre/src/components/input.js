import React from 'react';

const Input = ({value, setValue, label, error}) => (
  <input
    value={value}
    setValue={(e) => setValue(e.target.value)}
    placeholder={label}
    style={{borderColor: error ? 'red' : 'black'}}
  />
);

export default Input;
